export const COLOR = {
    BLUE_COLOR: "#a8dadc",
    DARK_BLUE_COLOR: "#98c1d9",
    GREEN_COLOR: "#90be6d",
    RED_COLOR: "#ef233c",
    GREY_COLOR: "#8d99ae",
};