import React from 'react';
import {Modal, View, Text, Button, StyleSheet} from 'react-native';
import {COLOR} from "../color";

const ModalShow = ({visible, onCancel, onOk}) =>{

    return(
        <Modal visible={visible}>
            <View style={styles.modalWindow}>
                <Text>Вас пост опубликован</Text>
                <View style={styles.buttonContainer}>
                    <Button
                        title = 'Отмена'
                        onPress={onCancel}
                        color={COLOR.RED_COLOR}
                    />
                    <Button
                        title = 'OK'
                        onPress={onOk}
                        color={COLOR.GREEN_COLOR}
                    />
                </View>
            </View>
        </Modal>
    )
};

export default ModalShow;

const styles = StyleSheet.create({
    modalWindow:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonContainer:{
        margin: 10,
        flexDirection: 'row',
        width: "100%",
        justifyContent: 'space-around'
    }
});