import React, {useState, useEffect} from 'react';
import {View, Text, Button, Image, StyleSheet, ScrollView} from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import {COLOR} from "../color";

export default function PostScreen({post, goBack}){

    const [like, setLike] = useState(0);

    useEffect(()=>{
        fetch(`https://newstime-f47b9.firebaseio.com/posts/${post.id}/postLike.json`, {
            method: 'GET',
            headers: {'Content-Type': 'application/json'}
        }).then((response) => {
            return response.json();
        }).then((data)=>{
            setLike(data.like);
        })
    });

    const likeHandle = async () =>{

          setLike(+ 1);

          const likePlus = {
              like: like + 1
          };
          fetch(`https://newstime-f47b9.firebaseio.com/posts/${post.id}/postLike/.json`, {
              method: 'PATCH',
              body: JSON.stringify(likePlus)
          }).then((response)=>{
              return response.json()
          });
    };

    const dizLike = async () =>{
            setLike(-1);

            const likePlus = {
                like: like - 1
            };
            fetch(`https://newstime-f47b9.firebaseio.com/posts/${post.id}/postLike/.json`, {
                method: 'PATCH',
                body: JSON.stringify(likePlus)
            }).then((response) => {
                return response.json()
            });
    };


    return(
        <ScrollView>
            <View>
                <AntDesign name="arrowleft" size={35} color="black" onPress={goBack}/>
            </View>
            <View style={styles.headerPost}>
                <Image source={{uri: post.url}} style={styles.img}/>
                <View style={styles.titlePost}>
                    <Text style={styles.title}>{post.title}</Text>
                </View>
            </View>
            <View style={styles.textContainer}>
                <Text style={styles.text}>{post.text}</Text>
            </View>
            <View style={styles.blogContainer}>
                <Text style={styles.blog}>{post.blog}</Text>
            </View>
            <View>
                <View style={styles.likeContainer}>
                    <Text>Нравится: {like}</Text>
                </View>
                <Button title='Нравится' onPress={likeHandle} color={COLOR.GREEN_COLOR}/>
                <Button title='Не нравится' onPress={dizLike} color={COLOR.RED_COLOR}/>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    headerPost:{
        marginTop: 5,
        width: 390,
    },
    img:{
        height: 300,
    },
    titlePost:{
        backgroundColor: '#F4FAFF',
    },
    title:{
        width: 385,
        padding: 10,
        fontSize: 25,
        fontWeight: 'bold'
    },
    textContainer:{
        width: 385,
        padding: 10,
        backgroundColor: '#A6A2A2'
    },
    text:{
        fontSize: 18,
        fontWeight: 'normal'
    },
    blogContainer:{
        width: 385,
        padding: 10,
    },
    blog:{
        fontSize: 15
    },
    likeContainer:{
        padding: 10
    }
});