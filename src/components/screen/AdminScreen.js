import React from 'react';
import {View, StyleSheet, Text, TextInput, Alert, Image, ScrollView, Button} from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import {NavBar} from "../Navbar/Navbar";
import FlatButton from "../button/button";
import firebase from "firebase";
import {storage} from "../../../firebaseConfig";
import {COLOR} from "../color";
import ModalShow from "../Modal/Modal";

export default  class Adminpage extends React.Component{
    state = {
        title: "",
        text: "",
        url: "",
        blog: "",
        image: null,
        ModalShow: false,
    };

    titleValueChanged = (title) =>{
        this.setState({
            title: title
        })
    };

    textValueChanged = (text) =>{
        this.setState({
            text: text
        })
    };

    blogValueChanged = (blog) =>{
        this.setState({
            blog: blog
        })
    };

    chooseImage = async () =>{
        if (Constants.platform.ios) {
            const { status } = await ImagePicker.requestCameraRollPermissionsAsync();
            if (status !== 'granted') {
                alert('Sorry, we need camera roll permissions to make this work!');
            }
        }

        const result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
        });


        this.setState({
            image: result.uri
        })

    };

    uploadImage = async uri =>{
      const patch = `images/${Date.now()}`;
        return new Promise (async (res, response)=>{
            const data = await fetch(uri);
            const file = await data.blob();
            const upload = firebase.storage().ref(patch).put(file);
            upload.on('state_changed', snapshot=>{

            },
            error=>{
                response(error)
            }, async ()=>{
                const url = await upload.snapshot.ref.getDownloadURL().then(url=>{
                    this.setState({
                        url: url
                    })
                });
                res(url)
            })
        })
    };

    saveImage = async (localUrl) =>{
        const remoteUrl = await this.uploadImage(localUrl);
        return new Promise((res, response)=>{
            firebase.database().ref('/images').push({
                timestamp: this.timestamp,
                image: remoteUrl
            });
            this.firestore.collection('images').add({
                timestamp: this.timestamp,
                image: remoteUrl
            }).then(ref=>{
                res(ref);
            }).catch(error=>{
                response(errors)
            })
        })
    };

    upload = () =>{
      this.uploadImage(this.state.image).then(()=>{

      }).catch(error=>{
          Alert.alert(error)
      })
    };

    selectAdmin = () =>{
        const postAdmin = {
            url: this.state.url,
            text: this.state.text,
            title: this.state.title,
            blog: this.state.blog,
            postLike: {
                like: 0,
                status: false,
                dizLike: 0
            }
        };

        if (this.state.title === ''){
            return false
        }

        fetch(`https://newstime-f47b9.firebaseio.com/kgnews.json`, {
            method: "POST",
            body: JSON.stringify(postAdmin)
        }).finally(() => {
            this.setState({
                ModalShow: true
            });
        })

    };


    pushPostServer = async () =>{
        const postAdmin = {
            url: this.state.url,
            text: this.state.text,
            title: this.state.title,
            blog: this.state.blog,
            postLike: {
                like: 0,
                status: false,
                dizLike: 0
            }
        };

        if (this.state.title === ''){
            return false
        }


        await fetch(`https://newstime-f47b9.firebaseio.com/posts.json`, {
            method: "POST",
            body: JSON.stringify(postAdmin)
        }).finally(() => {
            this.setState({
                ModalShow: true
            });
        })
    };

    render() {
        return (
            <ScrollView>
                <ModalShow
                    visible={this.state.ModalShow}
                    onCancel={()=>{
                        this.setState({
                            ModalShow: false
                        })
                    }}
                    onOk={()=>{
                        this.setState({
                            ModalShow: false
                        })
                    }}
                />
                <NavBar title='Kara-Balta News Time'/>
                <Text style={styles.titleScreen}>
                    Создание нового поста
                </Text>
                <View style={styles.box}>
                    <View>
                        <TextInput
                            style={styles.textInput}
                            placeholder="Название загаловки"
                            name="title"
                            onChangeText={this.titleValueChanged}
                            value={this.state.title}
                        />
                    </View>
                    <View>
                        <TextInput
                            style={styles.textInput}
                            placeholder="Текст под загаловки"
                            name="text"
                            onChangeText={this.textValueChanged}
                            value={this.state.text}
                        />
                    </View>
                    <View>
                        <TextInput
                            style={styles.textInputBlog}
                            placeholder="Блог"
                            multiline={true}
                            numberOfLines={10}
                            name='blog'
                            onChangeText={this.blogValueChanged}
                            value={this.state.blog}
                        />
                    </View>
                    <View>
                        <View style={styles.buttonBox}>
                            <FlatButton
                                text="Выбрать картинку"
                                onPress={this.chooseImage}
                            />
                            <FlatButton
                                text="Сохранить на сервер"
                                onPress={this.upload}
                            />
                        </View>
                        <View style={styles.ImageBox}>
                            {this.state.image && <Image
                                source={{ uri: this.state.image }}
                                style={{
                                    marginBottom: 10,
                                    width: 350,
                                    height: 200,
                                    borderWidth: 2,
                                    borderColor: COLOR.GREY_COLOR,
                                }} />
                            }
                        </View>
                    </View>
                    <View style={styles.buttonBox}>
                        <FlatButton
                            onPress={this.pushPostServer}
                            text="Опубликовать пост"
                        />
                        <View style={styles.postBtn}>
                            <Button
                                onPress={this.selectAdmin}
                                title="Опубликовать пост в Кыргызстане"
                                color={COLOR.GREEN_COLOR}
                            />
                        </View>
                    </View>
                </View>
            </ScrollView>
        )
    }
};

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    titleScreen:{
      fontSize: 25,
      textAlign: "center"
    },
    box:{
      marginTop: 15
    },
    textInput:{
        width: 370,
        margin: 10,
        padding: 10,
        borderWidth: 1,
        borderColor: "#E0E0E0"
    },
    textInputBlog:{
        width: 370,
        textAlignVertical: 'top',
        margin: 10,
        height: 150,
        padding: 10,
        borderWidth: 1,
        borderColor: "#E0E0E0"
    },
    buttonBox:{
        flexDirection: "row",
        justifyContent: 'space-between'
    },
    ImageBox:{
        alignItems: 'center',
        justifyContent: 'center'
    },
    postBtn:{
        marginRight: 10,
        width: 150,
        height: 85,
    }
});