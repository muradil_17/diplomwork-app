import React, {useState, useEffect} from 'react';
import {View, Text, TouchableOpacity, Image, StyleSheet, FlatList, ScrollView} from 'react-native';
import {NavBar} from "../Navbar/Navbar";
import {FontAwesome} from "@expo/vector-icons";
import {COLOR} from "../color";
import KGPostScreen from "./KGPostScreen";

const KGNewsScreen = () => {
    const [post, setPost] = useState([]);
    const [postId, setPostID] = useState(0);
    const [postScreen, setPostScreen] =useState('');

    useEffect(() =>{
        fetch('https://newstime-f47b9.firebaseio.com/kgnews.json',
            {
                method: 'GET',
                headers: {'Content-Type': 'application/json'}
            })
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                const arr = Object.keys(data).map(key => ({...data[key], id: key}));
                setPost(arr);
                setPostID(arr.length)
            });
    });

    const reversePost = post.reverse();

    let nextPostScreen = (
        <FlatList
            data={reversePost}
            renderItem ={({item})=>(
                <TouchableOpacity
                    activeOpacity={0.5}
                    onPress={()=> nextScreen(item.id)}
                    onLongPress={() => removePost(item.id)}
                >
                    <View style={styles.box}>
                        <Image source={{uri: item.url}} style={styles.img}/>
                        <View style={styles.text}>
                            <Text style={styles.title}>{item.title}</Text>
                            <Text style={styles.direct}>{item.text}</Text>
                        </View>
                        <View style={styles.button}>
                            <FontAwesome.Button
                                style={styles.remove}
                                onPress={() => removePost(item.id)}
                                name='remove'
                                size={25}
                            />
                        </View>
                    </View>
                </TouchableOpacity>
            )}
        />
    );

    const nextScreen = (id) =>{
        setPostScreen(id);
    };

    if(postScreen) {
        const selectPost = post.find(post=> post.id === postScreen);
        nextPostScreen = <KGPostScreen post={selectPost} goBack={()=> setPostScreen('')}/>
    }

    const removePost = (id) =>{
        const index = post.findIndex(post => post.id === id);
        post.splice(index, 1);

        setPost(post);
        setPostID(postId - 1);

        fetch(`https://newstime-f47b9.firebaseio.com/kgnews/${id}.json`, {
            method: "DELETE",
        }).then()
    };


    return(
        <ScrollView>
            <NavBar title='Kara-Balta News Time'/>
            <View style={styles.headerText}>
                <Text style={styles.headText}>В Кыргызстане</Text>
            </View>
            {nextPostScreen}
        </ScrollView>
    )
};
export default KGNewsScreen;

const styles = StyleSheet.create({
    headerText:{
        margin: 10
    },
    headText:{
      fontSize: 20
    },
    container:{
        margin: 2,
    },
    box:{
        marginTop: 5,
        marginBottom: 5,
        flexDirection: "row",
        width: "100%",
        padding: 5,
        borderWidth: 2,
        borderRadius: 5,
        borderColor: "#E0E0E0"
    },
    img:{
        width: 90,
        height: 100,
    },
    text:{
        width: 240,
        marginLeft: 15
    },
    title:{
        width: 220,
        fontSize: 17,
        fontWeight: "bold"
    },
    direct:{
        width: 250,
        fontSize: 12
    },
    button:{
        height: 25,
        width: 35,
    },
    remove:{
        margin: 0,
        padding: 5,
        paddingLeft: 5,
        paddingRight: 0,
        backgroundColor: COLOR.RED_COLOR
    }
});