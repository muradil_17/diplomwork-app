import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from "react-native";
import {COLOR} from "../color";

export default function flatButton({text, onPress}) {
    return(
        <TouchableOpacity onPress={onPress}>
            <View style={styles.button}>
                <Text style={styles.buttonText}>{text}</Text>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
   button:{
       width: 150,
       height: 55,
       padding: 10,
       margin: 10,
       backgroundColor: COLOR.GREEN_COLOR,
       borderRadius: 5,
   },
   buttonText:{
       color: "#FFF",
       fontSize: 15,
       fontWeight: "bold",
       textAlign: "center"
   }
});