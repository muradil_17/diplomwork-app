import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AdminScreen from './../screen/AdminScreen';
import KGNewsScreen from './../screen/kgnewsScreen';
import MainScreen from './../screen/MainScreen';



const MaterialBottomTabs = createMaterialBottomTabNavigator();

export default class AppNavigator extends React.Component{

    render() {
        return(
            <NavigationContainer>
                <MaterialBottomTabs.Navigator
                    path='/'
                    initialRouteName="Home"
                    activeColor="#000"
                    inactiveColor="#A2AEBB"
                    barStyle={{ backgroundColor: '#fff' }}
                    tabBarOptions={{
                        renderIndicator: false
                    }}
                >
                    <MaterialBottomTabs.Screen
                        options={{
                            tabBarLabel: 'Новости',
                            tabBarIcon: ({color}) =>(
                                <MaterialCommunityIcons name="format-list-bulleted" color={color} size={26}/>
                            ),
                        }}
                        name="Новости"
                        component={MainScreen}

                    />
                    <MaterialBottomTabs.Screen
                        options={{
                            tabBarLabel: 'В кыргызстане',
                            tabBarIcon: ({color}) =>(
                                <MaterialCommunityIcons name="map-marker-multiple" color={color} size={26}/>
                            )
                        }}
                        name='В кыргызстане'
                        component={KGNewsScreen}
                    />
                    <MaterialBottomTabs.Screen
                        options={{
                            tabBarLabel: 'Администрация',
                            tabBarIcon: ({color}) =>(
                                <MaterialCommunityIcons name="database-plus" color={color} size={26}/>
                            )
                        }}
                        name='Администрация'
                        component={AdminScreen}
                    />
                </MaterialBottomTabs.Navigator>
            </NavigationContainer>
        )
    }
}